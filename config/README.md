# Anthos Configuration Management Directory

This is the root directory for Anthos Configuration Management.

# Kubevirt demo

>NOTE: this `kubevirt` demo is setup for a virtual-in-virtual VM using `qemu` emulation for a test VM.

1. Install `virtctl` via `krew` (NOTE: Krew needs to be installed first)
    ```bash
    kubectl krew install virt
    kubectl virt help
    ```
1. Change line 9 in `/config/namespaces/customers/customer-3/virtual-machine.yaml` to "true" to run the VM
1. Check on status of VM
    ```bash
    kubectl get all -n cust-vm
    ```
    >NOTE: Look for "vms"

    ```bash
    kubectl get vmis
    ```
    >LOOK: Loo for the `cust-vm` with state "running"
1. Log into VM:
    ```bash
    kubectl virt console cust-vm
    # Hit "enter" a few times to see the prompt, username and password...login
    ```
1. Log out of shell: Hit `<ctrl>+]`

1. Stop VM by editing line 9 in `/config/namespaces/customers/customer-3/virtual-machine.yaml` to "false" to stop the VM


## Known "gotchas"

* Everything tries to be consistent at the same time, so CRDs are not always installed before the VMs templates are added (ie, `/config/namespaces/customers/customer-3/virtual-machine.yaml`)

    >FIX: Comment out all lines of the file: `/config/namespaces/customers/customer-3/virtual-machine.yaml`, commit & push...wait for CRD and operator to install (usually about 2 min), then uncomment and commit...
    >FIX: This is a workaround, the ACM **should** become eventually consistent, but this doesn't appear to always happen



See [our documentation](https://cloud.google.com/anthos-config-management/docs/repo) for how to use each subdirectory.
