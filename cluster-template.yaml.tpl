kind: Cluster
apiVersion: clusterregistry.k8s.io/v1alpha1
metadata:
  # Corresponds to the cluster's ConfigManagement configuration and must be unique within Environ
  name: %%CLUSTER_NAME%%
  labels:
    environment: dev
    type: BYOK
    customer: %%CUSTOMER%%
    cluster-name: %%CLUSTER_NAME%%